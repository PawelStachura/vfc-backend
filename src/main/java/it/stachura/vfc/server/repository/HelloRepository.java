package it.stachura.vfc.server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.stachura.vfc.server.documents.Hello;

@Repository
public interface HelloRepository extends CrudRepository<Hello, Long>{

}
