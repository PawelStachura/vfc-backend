package it.stachura.vfc.server.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import it.stachura.vfc.server.documents.Hello;
import it.stachura.vfc.server.repository.HelloRepository;
import it.stachura.vfc.server.service.HelloService;

@Service
public class HelloServiceImpl implements HelloService{

	@Resource
	HelloRepository helloRepository;
	
	@Override
	public Hello getHello() {
		Hello hello = new Hello("helo");
		helloRepository.save(new Hello("helo"));
		return hello;
	}
	
}
